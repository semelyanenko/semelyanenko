package com.training.emelyanenko;

import com.training.emelyanenko.atm.MoneyConsumer;
import com.training.emelyanenko.atm.MoneyProducer;
import com.training.emelyanenko.card.AbstractCard;
import com.training.emelyanenko.card.CreditCard;
import com.training.emelyanenko.util.RandomUtil;

import java.math.BigDecimal;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Class to run the program.
 */
public class Main {
    /**
     * An entry point to the program.
     *
     * @param args args it's array of program arguments.
     */
    public static void main(String[] args) {
        AbstractCard card = new CreditCard("Stas", new BigDecimal("500"));
        Random rand = new Random();
        int index = RandomUtil.rand(3, 5);

        ExecutorService executorService = Executors.newFixedThreadPool(index * 2);
        synchronized (Main.class) {
            for (int i = 0; i < index; i++) {
                executorService.submit(new MoneyConsumer(card));
                executorService.submit(new MoneyProducer(card));
            }
            try {
                Main.class.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            executorService.shutdown();
        }

        System.out.println("The program has finished. The balance is - " + card.getBalance());
    }
}