package com.training.emelyanenko.util;

import java.util.Random;

/**
 * Generator of random numbers within [min, max].
 */
public class RandomUtil {

    private RandomUtil() {
    }

    /**
     * Generates random int within [min, max].
     *
     * @param min it's the bottom border.
     * @param max it's the top border.
     * @return random number within [min, max].
     */
    public static int rand(int min, int max) {
        Random rnd = new Random();
        return rnd.nextInt(max - min + 1) + min;
    }
}
