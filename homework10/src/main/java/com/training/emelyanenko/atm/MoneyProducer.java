package com.training.emelyanenko.atm;

import com.training.emelyanenko.card.AbstractCard;
import com.training.emelyanenko.util.RandomUtil;

/**
 * Uses the card to replenish the account periodically (once in 2-5 seconds)
 * for a certain amount (5-15 dollars). A message about this event and the
 * current account status is displayed on the screen.
 */
public class MoneyProducer extends Atm implements Runnable {

    public MoneyProducer(AbstractCard card) {
        super(card);
    }

    /**
     * Periodically (once in 2-5 seconds) adds a certain
     * amount from the account (5-10 dollars)
     * until the balance becomes negative or exceeds 1000.
     */
    public void run() {
        while (!isLess() && !isGreater()) {
            addToBalance(card, Integer.toString(RandomUtil.rand(5, 15)));
            try {
                Thread.sleep(RandomUtil.rand(2000, 5000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
