package com.training.emelyanenko.atm;

import com.training.emelyanenko.Main;
import com.training.emelyanenko.card.AbstractCard;
import com.training.emelyanenko.util.RandomUtil;

/**
 * Uses the card in order to periodically (once in 2-5 seconds) withdraw
 * a certain amount from the account (5-10 dollars). A message about
 * this event and the current account status is displayed on the screen.
 */
public class MoneyConsumer extends Atm implements Runnable {
    public MoneyConsumer(AbstractCard card) {
        super(card);
    }

    /**
     * Periodically (once in 2-5 seconds) withdraw
     * a certain amount from the account (5-10 dollars)
     * until the balance becomes negative or exceeds 1000.
     */
    public void run() {
        while (!isLess() && !isGreater()) {
            withdraw(card, Integer.toString(RandomUtil.rand(5, 10)));
            try {
                Thread.sleep(RandomUtil.rand(2000, 5000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        synchronized (Main.class) {
            Main.class.notify();
        }
    }
}