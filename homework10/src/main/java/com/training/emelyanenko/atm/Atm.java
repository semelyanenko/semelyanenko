package com.training.emelyanenko.atm;

import com.training.emelyanenko.card.AbstractCard;

import java.math.BigDecimal;

/**
 * ATM class uses the card, allows operations of refilling, withdrawing, viewing the balance.
 */
public class Atm {

    protected AbstractCard card;

    public Atm(AbstractCard card) {
        if (card == null) {
            throw new RuntimeException("Card didn't insert");
        }
        this.card = card;
    }

    /**
     * Show balance.
     *
     * @return balance value.
     */
    public static BigDecimal showBalance(AbstractCard card) {
        return card.getBalance();
    }

    /**
     * Add cash value to balance.
     *
     * @param card  it's card for operations.
     * @param value value which adds to balance.
     */
    public static void addToBalance(AbstractCard card, String value) {
        isValid(card, value);
        card.addToBalance(new BigDecimal(value));
    }

    /**
     * Withdraw money from balance.
     *
     * @param card  it's card for operations.
     * @param value value which withdraws from balance.
     */
    public static BigDecimal withdraw(AbstractCard card, String value) {
        isValid(card, value);
        return card.withdraw(new BigDecimal(value));
    }

    /**
     * @param card  it's card for operations.
     * @param value string value of cash.
     * @return true if params is valid.
     */
    private static boolean isValid(AbstractCard card, String value) {
        if (value == null || card == null) {
            throw new RuntimeException("Insert correct values");
        }
        return true;
    }

    /**
     * @return true if the balance is less than 0.
     */
    protected boolean isLess() {
        return showBalance(card).compareTo(new BigDecimal("0.00")) <= 0;
    }

    /**
     * @return true if the balance is greater than 1000.
     */
    protected boolean isGreater() {
        return showBalance(card).compareTo(new BigDecimal("1000.00")) > 0;
    }
}