package com.training.emelyanenko.card;

import java.math.BigDecimal;

/**
 * Abstract class which stores card holder name and account balance.
 */
public abstract class AbstractCard {

    private String ownerName;
    private BigDecimal balance;

    /**
     * Sets "Unknown' user name if name is null.
     *
     * @param ownerName it's card owner name.
     * @param balance   it's initial balance value.
     */
    public AbstractCard(String ownerName, BigDecimal balance) {
        isValidCash(balance);
        this.balance = correctScale(balance);
        this.ownerName = (ownerName == null) ? "Unknown" : ownerName;
    }

    public AbstractCard(String ownerName) {
        this(ownerName, BigDecimal.ZERO);
    }

    public synchronized BigDecimal getBalance() {
        return balance;
    }

    public String getOwnerName() {
        return ownerName;
    }

    /**
     * Adds cash value to balance.
     *
     * @param cash is adds to balance.
     */
    public synchronized void addToBalance(BigDecimal cash) {
        if (isValidCash(cash)) {
            System.out.print("balance is " + getBalance() + " - ");
            balance = balance.add(correctScale(cash));
            balance = correctScale(balance);
            System.out.println("added " + cash + " " + Thread.currentThread().getName());
        }
    }

    /**
     * Withdraws cash value from the balance.
     *
     * @param cash is withdrawal value.
     * @return money that was withdrawn from the card.
     */
    public synchronized BigDecimal withdraw(BigDecimal cash) {
        if (isValidCash(cash)) {
            System.out.print("balance is " + getBalance() + " - ");
            balance = correctScale(doWithdraw(cash));
            System.out.println("withdrawn " + cash + " " + Thread.currentThread().getName());
        }
        return cash;
    }

    /**
     * Returns true if cash is valid.
     *
     * @param cash is argument for validation.
     * @return true if cash is valid.
     */
    private boolean isValidCash(BigDecimal cash) {
        if (cash == null || correctScale(cash).compareTo(BigDecimal.ZERO) == -1) {
            throw new RuntimeException("Insert correct value");
        }
        return true;
    }

    /**
     * Corrects cash scale.
     *
     * @param cash cash value with incorrect scale.
     * @return corrected cash value.
     */
    protected BigDecimal correctScale(BigDecimal cash) {
        return cash.setScale(2, BigDecimal.ROUND_CEILING);
    }

    protected abstract BigDecimal doWithdraw(BigDecimal cash);
}
