package com.training.emelyanenko.card;

import java.math.BigDecimal;

/**
 * Сredit card allows withdrawals, even if the balance is not positive.
 */
public class CreditCard extends AbstractCard {

    public CreditCard(String ownerName, BigDecimal balance) {
        super(ownerName, balance);
    }

    public CreditCard(String ownerName) {
        super(ownerName);
    }

    @Override
    protected synchronized BigDecimal doWithdraw(BigDecimal cash) {
        return getBalance().subtract(correctScale(cash));
    }

}
