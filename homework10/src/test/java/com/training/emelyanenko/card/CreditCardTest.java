package com.training.emelyanenko.card;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class CreditCardTest {

    private AbstractCard card;

    @Before
    public void setUp() {
        card = new CreditCard("Stas", new BigDecimal("100"));
    }

    @Test
    public void addToBalance() {
        card.addToBalance(new BigDecimal("100.335"));
        assertEquals(new BigDecimal("200.34"), card.getBalance());
    }

    @Test(expected = RuntimeException.class)
    public void addIncorrectValue() {
        card.addToBalance(new BigDecimal("-0.01111111111"));
        assertEquals(new BigDecimal("100.00"), card.getBalance());
    }

    @Test
    public void withdraw() {
        card.withdraw(new BigDecimal("50.589"));
        assertEquals(new BigDecimal("49.41"), card.getBalance());
    }

    @Test
    public void getBalance() {
        assertEquals(new BigDecimal("100.00"), card.getBalance());
    }

    @Test
    public void getOwnerName() {
        assertEquals("Stas", card.getOwnerName());
    }
}