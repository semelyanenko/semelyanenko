package com.training.emelyanenko.atm;

import com.training.emelyanenko.card.AbstractCard;
import com.training.emelyanenko.card.CreditCard;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class AtmTest {
    private AbstractCard crediCard;

    @Before
    public void setUp() {
        crediCard = new CreditCard("Stas", new BigDecimal("100"));
    }

    @Test
    public void showBalance() {
        assertEquals(Atm.showBalance(crediCard), new BigDecimal("100.00"));
    }

    @Test
    public void addToBalance() {
        crediCard.addToBalance(new BigDecimal("100"));
        assertEquals(Atm.showBalance(crediCard), new BigDecimal("200.00"));
    }

    @Test
    public void creditWithdraw() {
        crediCard = new CreditCard("Stas", new BigDecimal("100"));
        Atm.withdraw(crediCard, "200");
        assertEquals(Atm.showBalance(crediCard), new BigDecimal("-100.00"));
    }
}