package com.training.emelyanenko.task2;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class DiscreteMathTest {

    Set<String> set1;
    Set<String> set2;
    DiscreteMath<String> discreteMath = new DiscreteMath<>();


    @Before
    public void setUp() {
        set1 = new DiscreteMath<>();
        set2 = new DiscreteMath<>();
        set1.add("A");
        set1.add("B");
        set2.add("B");
        set2.add("C");
    }


    @Test
    public void union() {
        Set<String> expSet = new DiscreteMath<>();
        expSet.add("A");
        expSet.add("B");
        expSet.add("C");
        Set<String> union = discreteMath.union(set1, set2);
        assertArrayEquals(expSet.toArray(), union.toArray());
    }

    @Test
    public void intersection() {
        Set<String> expSet = new HashSet<>();
        expSet.add("B");
        Set<String> intersection = discreteMath.intersection(set1, set2);
        assertArrayEquals(expSet.toArray(), intersection.toArray());
    }

    @Test
    public void difference() {
        Set<String> expSet = new HashSet<>();
        expSet.add("A");
        Set<String> difference = discreteMath.difference(set1, set2);
        assertArrayEquals(expSet.toArray(), difference.toArray());
    }

    @Test
    public void complement() {
        Set<String> expSet = new HashSet<>();
        expSet.add("A");
        expSet.add("C");
        Set<String> complement = discreteMath.complement(set1, set2);
        assertArrayEquals(expSet.toArray(), complement.toArray());
    }
}