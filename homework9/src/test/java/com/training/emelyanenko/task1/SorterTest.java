package com.training.emelyanenko.task1;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class SorterTest {

    @Test
    public void sort() {
        int[] arr = new int[]{57, 1, 10, 301, 5, 90};
        int[] expArr = new int[]{1, 10, 301, 5, 90, 57};
        Sorter.sort(arr);
        assertArrayEquals(expArr, arr);
    }

    @Test
    public void negativeSort() {
        int[] arr = new int[]{-57, 1, -10, 301, 5, -90};
        int[] expArr = new int[]{1, -10, 301, 5, -90, -57};
        Sorter.sort(arr);
        assertArrayEquals(expArr, arr);
    }
}