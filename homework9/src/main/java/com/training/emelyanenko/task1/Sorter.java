package com.training.emelyanenko.task1;

/**
 * Class for sorting arrays of integers by sum of the digits
 * of its numbers via Selection sort method.
 */
public class Sorter {

    private Sorter() {

    }

    /**
     * Sorts an array of integers by sum of the digits
     * of its numbers via Selection sort method.
     *
     * @param array it's an array to sort.
     */
    public static void sort(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            int min = i;
            for (int j = i + 1; j < array.length; j++) {
                if (numSum(array[j]) < numSum(array[min])) {
                    min = j;
                }
            }
            int temp = array[min];
            array[min] = array[i];
            array[i] = temp;
        }
    }

    /**
     * Calculates sum of number's digits.
     *
     * @param number it's number for calculation.
     * @return sum of number's digits.
     */
    private static int numSum(int number) {
        int index = 0;
        int sum = 0;
        String[] numArr = Integer.toString(number).split("");
        if (numArr[0].equals("-")) {
            index = 1;
        }
        for (; index < numArr.length; index++) {
            sum += Integer.parseInt(numArr[index]);
        }
        return sum;
    }
}
