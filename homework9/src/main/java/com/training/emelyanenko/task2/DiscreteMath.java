package com.training.emelyanenko.task2;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * HashSet implementation with general discrete math methods.
 *
 * @param <E> it's type of element.
 */
public class DiscreteMath<E> extends HashSet<E> {

    /**
     * Returns union collection.
     *
     * @param set1 first collection.
     * @param set2 second collection.
     * @return union collection.
     */
    public Set<E> union(Set<E> set1, Set<E> set2) {
        Set<E> unionSet = new HashSet<>();
        unionSet.addAll(set1);
        unionSet.addAll(set2);
        return unionSet;
    }

    /**
     * Returns intersection collection.
     *
     * @param set1 first collection.
     * @param set2 second collection.
     * @return intersection collection.
     */
    public Set<E> intersection(Set<E> set1, Set<E> set2) {
        Set<E> interSet = new HashSet<>();
        Iterator<E> iterator = set1.iterator();
        while (iterator.hasNext()) {
            E next = iterator.next();
            if (set2.contains(next)) {
                interSet.add(next);
            }
        }
        return interSet;
    }

    /**
     * Returns difference collection.
     *
     * @param set1 first collection.
     * @param set2 second collection.
     * @return difference collection.
     */
    public Set<E> difference(Set<E> set1, Set<E> set2) {
        Set<E> diffSet = new HashSet<>();
        Iterator<E> iterator = set1.iterator();
        while (iterator.hasNext()) {
            E next = iterator.next();
            if (!set2.contains(next)) {
                diffSet.add(next);
            }
        }
        return diffSet;
    }

    /**
     * Returns complement collection.
     *
     * @param set1 first collection.
     * @param set2 second collection.
     * @return complement collection.
     */
    public Set<E> complement(Set<E> set1, Set<E> set2) {
        Set<E> compSet = new HashSet<>();
        compSet.addAll(difference(set1, set2));
        compSet.addAll(difference(set2, set1));
        return compSet;
    }
}
