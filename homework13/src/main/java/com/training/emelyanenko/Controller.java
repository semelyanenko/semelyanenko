package com.training.emelyanenko;

import com.training.emelyanenko.exception.ValidationException;
import com.training.emelyanenko.service.HttpClientService;
import com.training.emelyanenko.service.JavaNetService;
import com.training.emelyanenko.util.Parser;

/**
 * Class for managing requests.
 */
public class Controller {

	private Controller() {
	}

	/**
	 * Executes the required method and returns the response depending on the arguments.
	 *
	 * @param args it's program arguments.
	 * @return response depending on the arguments.
	 */
	public static String control(String[] args) {
		isValid(args);
		if (args[0].equals("GET") && args[2].equals("1")) {
			return Parser.parseToOutPut(new JavaNetService().getArticle(Integer.parseInt(args[1])), "GET");
		}
		if (args[0].equals("GET") && args[2].equals("2")) {
			return Parser.parseToOutPut(new HttpClientService().getArticle(Integer.parseInt(args[1])), "GET");
		}
		if (args[0].equals("POST") && args[2].equals("1")) {
			Article article = new Article(1, Integer.parseInt(args[1]), "some title", "some body");
			return Parser.parseToOutPut(new JavaNetService().putArticle(article), "POST");
		}
		if (args[0].equals("POST") && args[2].equals("2")) {
			Article article = new Article(1, Integer.parseInt(args[1]), "some title", "some body");
			return Parser.parseToOutPut(new HttpClientService().putArticle(article), "POST");
		}
		throw new ValidationException("Program arguments is invalid");
	}

	/**
	 * Validates program arguments.
	 *
	 * @param args it's program arguments.
	 */
	private static void isValid(String[] args) {
		if (args == null || args.length != 3) {
			throw new ValidationException("The number of arguments must be 3");
		}
		if (!args[0].equals("GET") && !args[0].equals("POST")) {
			throw new ValidationException("HTTP method is incorrect");
		}
		if (!args[1].matches("[0-9]*") || !args[2].matches("[0-9]*")) {
			throw new ValidationException("Program arguments is invalid");
		}
	}
}
