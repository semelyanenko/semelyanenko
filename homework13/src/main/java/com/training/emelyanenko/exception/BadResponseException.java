package com.training.emelyanenko.exception;

public class BadResponseException extends RuntimeException {

	public BadResponseException(String message) {
		super(message);
	}
}
