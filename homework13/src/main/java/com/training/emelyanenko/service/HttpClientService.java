package com.training.emelyanenko.service;

import com.training.emelyanenko.Article;
import com.training.emelyanenko.exception.BadConnectionException;
import com.training.emelyanenko.exception.ServerException;
import com.training.emelyanenko.util.Parser;
import com.training.emelyanenko.util.ResponseReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;

/**
 * Service realization that uses apache.http functionality.
 */
public class HttpClientService implements Service {

	/**
	 * Uses GET HTPP method returns string representation of article with id.
	 *
	 * @param id it's article id.
	 * @return tring representation of article with id.
	 */
	@Override
	public String getArticle(int id) {
		String responseMessage;
		CloseableHttpClient client = HttpClientBuilder.create().build();
		HttpGet get = new HttpGet(URL + "/" + id);
		try {
			HttpResponse response = client.execute(get);
			responseMessage = ResponseReader.getResponse(response);
		} catch (IOException e) {
			e.printStackTrace();
			responseMessage = null;
		}
		if (responseMessage == null) {
			throw new BadConnectionException("Connection failed");
		}
		return responseMessage;
	}

	/**
	 * Puts article uses POST HTTP method.
	 *
	 * @param article it's article to put.
	 * @return response message.
	 */
	@Override
	public String putArticle(Article article) {
		String output;
		CloseableHttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(URL);
		post.setHeader("Content-Type", "application/json; charset=UTF-8");
		post.setEntity(new StringEntity(Parser.toJson(article), "UTF-8"));
		HttpResponse response;
		try {
			response = client.execute(post);
			if (response.getStatusLine().getStatusCode() != 201) {
				throw new ServerException(response.getStatusLine().toString() + "\n" + ResponseReader.getResponse(response));
			}
			output = Parser.toJson(article);
		} catch (IOException e) {
			e.printStackTrace();
			output = null;
		}
		if (output == null) {
			throw new BadConnectionException("Connection failed");
		}
		return output;
	}
}
