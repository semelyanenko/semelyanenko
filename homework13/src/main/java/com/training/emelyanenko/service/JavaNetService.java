package com.training.emelyanenko.service;


import com.training.emelyanenko.Article;
import com.training.emelyanenko.exception.BadConnectionException;
import com.training.emelyanenko.exception.BadResponseException;
import com.training.emelyanenko.util.Parser;
import com.training.emelyanenko.util.ResponseReader;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * Service realization that uses java.net functionality.
 */
public class JavaNetService implements Service {

	/**
	 * Uses GET HTPP method returns string representation of article with id.
	 *
	 * @param id it's article id.
	 * @return tring representation of article with id.
	 */
	@Override
	public String getArticle(int id) {
		URLConnection connection;
		String responseMessage;
		try {
			connection = new URL(URL + "/" + id).openConnection();
			responseMessage = ResponseReader.getResponse(connection);
		} catch (IOException e) {
			e.printStackTrace();
			responseMessage = null;
		}
		if (responseMessage == null) {
			throw new BadConnectionException("Connection failed");
		}
		return responseMessage;
	}

	/**
	 * Puts article uses POST HTTP method.
	 *
	 * @param article it's article to put.
	 * @return response message.
	 */
	@Override
	public String putArticle(Article article) {
		String output;
		HttpURLConnection connection = null;
		OutputStream os = null;
		try {
			URL url = new URL(URL);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			connection.setDoOutput(true);
			os = connection.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
			osw.write(Parser.toJson(article));
			osw.flush();
			osw.close();
			if (connection.getResponseCode() == HttpURLConnection.HTTP_CREATED) {
				output = Parser.toJson(article);
			} else {
				throw new BadResponseException(ResponseReader.getResponse(connection));
			}
		} catch (IOException e) {
			e.printStackTrace();
			output = null;
		} finally {
			try {
				if (os != null) {
					os.close();
				}
				if (connection != null) {
					connection.disconnect();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (output == null) {
			throw new BadConnectionException("Connection failed");
		}
		return output;
	}
}
