package com.training.emelyanenko.service;

import com.training.emelyanenko.Article;

/**
 * Service class that implements GET and POST functionality of 'https://jsonplaceholder.typicode.com'.
 */
public interface Service {

	String URL = "https://jsonplaceholder.typicode.com/posts";

	/**
	 * Uses GET HTPP method returns string representation of article with id.
	 *
	 * @param id it's article id.
	 * @return tring representation of article with id.
	 */
	String getArticle(int id);

	/**
	 * Puts article uses POST HTTP method.
	 *
	 * @param article it's article to put.
	 * @return response message.
	 */
	String putArticle(Article article);
}
