package com.training.emelyanenko.util;

import com.training.emelyanenko.Article;
import com.training.emelyanenko.exception.ValidationException;
import org.json.JSONObject;

import java.text.MessageFormat;

/**
 * Util class for parsing json data format.
 */
public class Parser {

	private Parser() {
	}

	/**
	 * Parse instance of Article class to json string.
	 *
	 * @param article it's article to parse.
	 * @return json format of article.
	 */
	public static String toJson(Article article) {
		if (article == null) {
			throw new IllegalArgumentException("Article must be initialized");
		}
		JSONObject jsonObject = new JSONObject(article);
		return jsonObject.toString();
	}

	/**
	 * Parse json server response to output string format.
	 *
	 * @param json   json server response.
	 * @param method http method.
	 * @return string in output format.
	 */
	public static String parseToOutPut(String json, String method) {
		if (json == null || method == null) {
			throw new IllegalArgumentException("Arguments cant be null");
		}
		JSONObject object = new JSONObject(json);
		if (!object.has("id") || !object.has("userId") || !object.has("title") || !object.has("body")) {
			throw new IllegalArgumentException("Json is invalid");
		}
		String id = object.get("id").toString();
		String userId = object.get("userId").toString();
		String title = object.getString("title");
		String body = object.getString("body");
		String output;
		if (method.equals("GET")) {
			output = "Article [{0}]: User [{1}] Title [ \"{2}\"] Message [\"{3}\"]";
		} else if (method.equals("POST")) {
			output = "Article [{0}] has been created: User [{1}] Title [ \"{2}\"] Message [\"{3}\"]";
		} else {
			throw new ValidationException("Http method is incorrect");
		}
		return MessageFormat.format(output, id, userId, title, body);
	}
}
