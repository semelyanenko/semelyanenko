package com.training.emelyanenko.util;

import com.training.emelyanenko.exception.BadResponseException;
import com.training.emelyanenko.exception.ServerException;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

import static org.junit.Assert.assertEquals;

public class ResponseReaderTest {

	private String url = "https://jsonplaceholder.typicode.com/posts/1";
	private String expectedResponse = "{  \"userId\": 1,  \"id\": 1,  \"title\": \"sunt aut facere repellat provident occaecati excepturi optio reprehenderit\",  \"body\": \"quia et suscipit\\nsuscipit recusandae consequuntur expedita et cum\\nreprehenderit molestiae ut ut quas totam\\nnostrum rerum est autem sunt rem eveniet architecto\"}";

	@Test
	public void getHttpResponse() {
		String responseMessage;
		CloseableHttpClient client = HttpClientBuilder.create().build();
		HttpGet get = new HttpGet(url);
		try {
			HttpResponse response = client.execute(get);
			responseMessage = ResponseReader.getResponse(response);
			assertEquals(expectedResponse, responseMessage);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Test(expected = ServerException.class)
	public void getHttpResponseWithServerException() {
		String responseMessage;
		CloseableHttpClient client = HttpClientBuilder.create().build();
		HttpGet get = new HttpGet(url + "01");
		try {
			HttpResponse response = client.execute(get);
			responseMessage = ResponseReader.getResponse(response);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Test
	public void getURLResponse() {
		URLConnection connection;
		String responseMessage;
		try {
			connection = new URL(url).openConnection();
			responseMessage = ResponseReader.getResponse(connection);
			assertEquals(expectedResponse, responseMessage);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test(expected = BadResponseException.class)
	public void getURLResponseWit() {
		URLConnection connection;
		String responseMessage;
		try {
			connection = new URL(url + "01").openConnection();
			responseMessage = ResponseReader.getResponse(connection);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}