package com.training.emelyanenko.util;

import com.training.emelyanenko.Article;
import org.json.JSONObject;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ParserTest {

	Article article = new Article(1, 1, "some title", "some body");

	@Test
	public void toJson() {

		String parsedToJson = Parser.toJson(article);
		assertEquals(new JSONObject(article).toString(), new JSONObject(article).toString());
	}

	@Test(expected = IllegalArgumentException.class)
	public void toJsonWithException() {
		Parser.toJson(null);
	}

	@Test
	public void parseGetToOutPut() {
		String output = Parser.parseToOutPut(new JSONObject(article).toString(), "GET");
		String expectedOutput = "Article [1]: User [1] Title [ \"some title\"] Message [\"some body\"]";
		assertEquals(expectedOutput, output);
	}

	@Test
	public void parsePostToOutPut() {
		String output = Parser.parseToOutPut(new JSONObject(article).toString(), "POST");
		String expectedOutput = "Article [1] has been created: User [1] Title [ \"some title\"] Message [\"some body\"]";
		assertEquals(expectedOutput, output);
	}

	@Test(expected = IllegalArgumentException.class)
	public void parseToOutPutWithIllegalExc() {
		String output = Parser.parseToOutPut(new JSONObject(article).toString(), null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void parseToOutWithIllegalArgExc() {
		String badJson = "{\"body\":\"some body\",\"title\":\"some title\",\"userId\":1}";
		Parser.parseToOutPut(badJson, "GET");
	}


}