package com.training.emelyanenko;

import com.training.emelyanenko.exception.ValidationException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ControllerTest {

	private String expectedGet = "Article [1]: User [1] Title [ \"sunt aut facere repellat provident occaecati excepturi optio reprehenderit\"] Message [\"quia et suscipit\n" +
			"suscipit recusandae consequuntur expedita et cum\n" +
			"reprehenderit molestiae ut ut quas totam\n" +
			"nostrum rerum est autem sunt rem eveniet architecto\"]";

	private String expectedPost = "Article [101] has been created: User [1] Title [ \"some title\"] Message [\"some body\"]";

	@Test
	public void controlGetFirst() {
		String actual = Controller.control(new String[]{"GET", "1", "1"});
		assertEquals(expectedGet, actual);
	}

	@Test
	public void controlGetSecond() {
		String actual = Controller.control(new String[]{"GET", "1", "2"});
		assertEquals(expectedGet, actual);
	}

	@Test
	public void controlPostFirst() {
		String actual = Controller.control(new String[]{"POST", "101", "1"});
		assertEquals(expectedPost, actual);
	}

	@Test
	public void controlPostSecond() {
		String actual = Controller.control(new String[]{"POST", "101", "2"});
		assertEquals(expectedPost, actual);
	}

	@Test(expected = ValidationException.class)
	public void controlWithValidationException() {
		Controller.control(new String[]{"POST", "101", "3"});
	}

	@Test(expected = ValidationException.class)
	public void controlWithIncorrectMethod() {
		Controller.control(new String[]{"PUT", "101", "1"});
	}

}