package com.training.emelyanenko.service;

import com.training.emelyanenko.Article;
import com.training.emelyanenko.exception.ServerException;
import org.json.JSONObject;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HttpClientServiceTest {

	@Test
	public void getArticle() {
		String json = new HttpClientService().getArticle(1);
		String sourceJson = "{\n" +
				"    \"userId\": 1,\n" +
				"    \"id\": 1,\n" +
				"    \"title\": \"sunt aut facere repellat provident occaecati excepturi optio reprehenderit\",\n" +
				"    \"body\": \"quia et suscipit\\nsuscipit recusandae consequuntur expedita et cum\\nreprehenderit molestiae ut ut quas totam\\nnostrum rerum est autem sunt rem eveniet architecto\"\n" +
				"  }";
		assertEquals(new JSONObject(sourceJson).toString(), new JSONObject(json).toString());
	}

	@Test(expected = ServerException.class)
	public void getArticleWithException() {
		String json = new HttpClientService().getArticle(102);
	}

	@Test
	public void putArticle() {
		Article article = new Article(1, 101, "some title", "some body");
		String response = new HttpClientService().putArticle(article);
		assertEquals(new JSONObject(article).toString(), new JSONObject(response).toString());
	}

	@Test(expected = ServerException.class)
	public void putArticleWithException() {
		Article article = new Article(1, 100, "some title", "some body");
		String response = new HttpClientService().putArticle(article);
		assertEquals(new JSONObject(article).toString(), new JSONObject(response).toString());
	}
}