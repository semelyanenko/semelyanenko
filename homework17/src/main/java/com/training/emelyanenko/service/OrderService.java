package com.training.emelyanenko.service;

import com.training.emelyanenko.domain.Order;
import com.training.emelyanenko.domain.Product;
import com.training.emelyanenko.exception.InvalidArgumentException;
import com.training.emelyanenko.repository.OrderRepository;
import com.training.emelyanenko.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Order service class.
 */
@Service
public class OrderService {

	@Autowired
	private OrderRepository orderRepository;
	@Autowired
	private UserRepository userRepository;

	/**
	 * Calculates total price of order.
	 *
	 * @param order order for calculation.
	 * @return total price.
	 */
	public double calcTotalPrice(Order order) {
		double totalPrice = 0.0;
		for (Product product : order.getProducts()) {
			totalPrice += product.getPrice();
		}
		return totalPrice;
	}

	/**
	 * Creates order and returns saved order.
	 *
	 * @param customer customer name.
	 * @return created order.
	 */
	public Order getOrder(String customer) {
		if (customer == null) {
			throw new InvalidArgumentException("Name can't be null");
		}
		if (userRepository.getByName(customer).isPresent() && userRepository.getByName(customer).get().getOrder() != null) {
			return userRepository.getByName(customer).get().getOrder();
		} else {
			return orderRepository.save(new Order(customer));
		}
	}
}
