package com.training.emelyanenko;

import com.training.emelyanenko.sql.SqlHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class Listener implements ServletContextListener {

	@Autowired
	AnnotationConfigApplicationContext annotationConfigApplicationContext;

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		SqlHelper.initDB();
		SpringContext.getApplicationContext();
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
	}
}
