package com.training.emelyanenko;

import com.training.emelyanenko.repository.OrderRepository;
import com.training.emelyanenko.repository.ProductRepository;
import com.training.emelyanenko.repository.UserRepository;
import com.training.emelyanenko.service.OrderService;
import com.training.emelyanenko.service.ProductService;
import com.training.emelyanenko.service.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.training.emelyanenko"})
public class AppConfig {


	@Bean
	OrderRepository orderRepository() {
		return new OrderRepository();
	}

	@Bean
	ProductRepository productRepository() {
		return new ProductRepository();
	}

	@Bean
	UserRepository userRepository() {
		return new UserRepository();
	}

	@Bean
	OrderService orderService() {
		return new OrderService();
	}

	@Bean
	ProductService productService() {
		return new ProductService();
	}

	@Bean
	UserService userService() {
		return new UserService();
	}
}
