package com.training.emelyanenko.controller;

import com.training.emelyanenko.SpringContext;
import com.training.emelyanenko.service.OrderService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Controller with mapping "/basket".
 */
@WebServlet(value = "/basket")
public class BasketServlet extends HttpServlet {

	private OrderService orderService;

	@Override
	public void init() {
		AnnotationConfigApplicationContext context = SpringContext.getApplicationContext();
		this.orderService = (OrderService) context.getBean("orderService");
	}

	/**
	 * Handles requests to the basket.
	 *
	 * @param req  it's http request.
	 * @param resp it's http response.
	 * @throws ServletException
	 * @throws IOException
	 */
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setAttribute("order", orderService.getOrder(req.getSession().getAttribute("customer").toString()));
		req.getRequestDispatcher("WEB-INF/jsp/receipt.jsp").forward(req, resp);
	}
}
