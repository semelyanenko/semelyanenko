package com.training.emelyanenko.repository;

import com.training.emelyanenko.domain.Order;
import com.training.emelyanenko.domain.Product;
import com.training.emelyanenko.sql.SqlHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Repository class that emulate data base and has some repository methods.
 */
@Repository
public class OrderRepository {

	private static Connection conn = SqlHelper.getConnection();

	@Autowired
	private ProductRepository productRepository;

	/**
	 * Saves order in database.
	 *
	 * @param order it's order to save.
	 * @return saved order.
	 */
	public Order save(Order order) {
		Connection conn = SqlHelper.getConnection();
		try (PreparedStatement ps = conn.prepareStatement("INSERT INTO SHOP_ORDER (id, user_id, total_price) VALUES (?,?,?)")) {
			ps.setInt(1, order.getId());
			ps.setInt(2, order.getUserId());
			ps.setDouble(3, order.getTotalPrice());
			ps.execute();
			for (Product product : order.getProducts()) {
				productRepository.saveOrderGood(product, order.getId());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return order;
	}
}
