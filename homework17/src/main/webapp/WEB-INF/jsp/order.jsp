<%@ page import="com.training.emelyanenko.SpringContext" %>
<%@ page import="com.training.emelyanenko.domain.Order" %>
<%@ page import="com.training.emelyanenko.service.OrderService" %>
<%@ page import="com.training.emelyanenko.service.ProductService" %>
<%@ page import="com.training.emelyanenko.service.UserService" %>
<%@ page import="org.springframework.context.annotation.AnnotationConfigApplicationContext" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<% AnnotationConfigApplicationContext context = SpringContext.getApplicationContext();
    UserService userService = (UserService) context.getBean("userService");
    ProductService productService = (ProductService) context.getBean("productService");
    OrderService orderService = (OrderService) context.getBean("orderService");
    Order order = orderService.getOrder(session.getAttribute("customer").toString());%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <style>
        <%@include file="/WEB-INF/css/style.css" %>
    </style>
    <title>Online-shop</title>
</head>
<body>
<div class="box">
    <p class="msg">Hello <%=order.getCustomer()%> !</p>
    <p class="msg">Make you order</p>
    <form method="post" action="product">
        <select name="selected">
            <c:forEach var="product" items="${products}">
                <option value="${product.key}">${product.key} (${product.value}$)</option>
            </c:forEach>
        </select>
        <input type="submit" value="Add item"/>
        <input type="hidden" name="id" value="<%= order.getId()%>"/>
        <input type="hidden" name="customer" value="<%= order.getCustomer()%>"/>
    </form>
    <form method="post" action="basket">
        <input type="submit" value="Submit">
        <input type="hidden" name="id" value="<%= order.getId()%>"/>
        <input type="hidden" name="customer" value="<%= order.getCustomer()%>"/>
    </form>
    <p>You have already chosen:</p>
    <c:forEach var="pickedProduct" items="${order.getProducts()}">
        <P>${pickedProduct.getName()} ${pickedProduct.getPrice()}</P>
    </c:forEach>
</div>
</body>
</html>

