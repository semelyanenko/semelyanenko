package com.training.emelyanenko.controller;

import com.training.emelyanenko.domain.PriceList;
import com.training.emelyanenko.service.OrderService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;

/**
 * Controller with mapping "/".
 */
public class WelcomeServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		resp.setCharacterEncoding("UTF-8");
		resp.setContentType("text/html; charset=UTF-8");
		Writer writer = resp.getWriter();
		writer.write("<!DOCTYPE html>\n" +
				"<html>\n" +
				"<head>\n" +
				"\t<meta charset=\"utf-8\">\n" +
				"\t<link rel=\"stylesheet\" type=\"text/css\" href=\"css/style.css\">\n" +
				"\t<title>Online-shop</title>\n" +
				"</head>\n" +
				"<body>\n" +
				"\t<div class=\"box\">\n" +
				"\t<p class=\"msg\">Welcome to Online Shop</p> \n" +
				"\t <form method=\"POST\"> \n" +
				"\t\t<input type=\"text\" name=\"customer\" placeholder=\"Enter your name\" />\n" +
				"</br>\t<input type=\"submit\" value=\"Enter\" />\n" +
				"\t</form>\n" +
				"</div>\n" +
				"</body>\n" +
				"</html>");
	}

	/**
	 * Makes forward to order.jsp if customer didn't select products to buying.
	 * Forwards to receipt.jsp if customer selected products.
	 *
	 * @param req  http request.
	 * @param resp http response.
	 * @throws ServletException
	 * @throws IOException
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (req.getParameter("selected") != null) {
			OrderService.addProducts(req.getParameter("id"), req.getParameterValues("selected"));
			req.getRequestDispatcher("WEB-INF/jsp/receipt.jsp").forward(req, resp);
		} else {
			req.setAttribute("products", PriceList.getPRODUCTS());
			req.getRequestDispatcher("WEB-INF/jsp/order.jsp").forward(req, resp);
		}
	}
}
