<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Online-Shop</title>
    <meta charset=\"utf-8\">
    <style>
        <%@include file="/WEB-INF/css/style.css" %>
    </style>
</head>
<body>
<div class="box">
    <p class="msg">Welcome to Online Shop</p>

    <form method="post" action="login">
        <input type="text" placeholder="Username" name="customer" required>
        <br>
        <input type="password" placeholder="Password" name="password" required>
        <br>
        <input type="checkbox" id="acceptTerms" name="acceptTerms" required/>
        <p>I agree with the terms of service</p>
        <input type="submit" value="Enter">
    </form>
</div>
</body>
</html>