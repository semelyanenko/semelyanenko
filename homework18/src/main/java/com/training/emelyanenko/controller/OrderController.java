package com.training.emelyanenko.controller;

import com.training.emelyanenko.domain.User;
import com.training.emelyanenko.service.OrderService;
import com.training.emelyanenko.service.ProductService;
import com.training.emelyanenko.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

@Controller
@RequestMapping("/product")
public final class OrderController {

    private UserService userService;
    private ProductService productService;

    @Autowired
    public OrderController(UserService userService, ProductService productService) {
        this.userService = userService;
        this.productService = productService;
    }

    @RequestMapping
    public String makeOrder(ModelMap model, Principal principal, @RequestParam(value = "selected", required = false) String[] selected) {
        User current = userService.createOrGet(principal.getName());
        if (selected != null) {
            productService.addProductToOrder(current.getOrder(), selected);
        }
        model.addAttribute("products", productService.getPriceList());
        model.addAttribute("order", current.getOrder());
        return "product";
    }
}
