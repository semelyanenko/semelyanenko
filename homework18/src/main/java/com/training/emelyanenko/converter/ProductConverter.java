package com.training.emelyanenko.converter;

import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Component
public class ProductConverter {

    public Map<String, Double> convertAll(ResultSet rs) {
        Map<String, Double> products = new HashMap<>();
        try {
            while (rs.next()) {
                products.put(rs.getString("title"), rs.getDouble("price"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return products;
    }

    public int convert(ResultSet rs) {
        int id = -1;
        try {
            if (!rs.next()) {
            }
            id = rs.getInt("id");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (id == -1) {
            throw new RuntimeException("Operation failed");
        }
        return id;
    }
}
