package com.training.emelyanenko.repository;

import com.training.emelyanenko.converter.UserConverter;
import com.training.emelyanenko.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

@Repository
public final class UserRepository {

    private static final String GET_BY_USER_NAME = "" +
            "SELECT * FROM USER user\n" +
            "LEFT JOIN SHOP_ORDER o\n" +
            "ON user.ID = o.USER_ID\n" +
            "LEFT JOIN ORDER_GOOD OG\n" +
            "ON o.ID = OG.ORDER_ID\n" +
            "LEFT JOIN GOOD good\n" +
            "ON OG.GOOD_ID = good.ID\n" +
            "WHERE user.LOGIN = ?";
    private static final String USER_SAVE_QUERY = "INSERT INTO USER (id, login, password) VALUES (?,?, ?)";


    private Connection connection;
    private UserConverter userConverter;

    @Autowired
    public UserRepository(Connection connection, UserConverter userConverter) {
        this.connection = connection;
        this.userConverter = userConverter;
    }

    /**
     * Saves user in "data base" (list of orders).
     *
     * @param user it's order to save.
     * @return saved user.
     */
    public User save(User user) {
        try (PreparedStatement ps = connection.prepareStatement(USER_SAVE_QUERY)) {
            ps.setInt(1, user.getId());
            ps.setString(2, user.getName());
            ps.setString(3, user.getPassword());
            ps.execute();
            userConverter.convertToDB(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    public Optional<User> getByName(String customer) {
        Optional<User> maybeUser = Optional.empty();
        ResultSet rs = null;
        try (PreparedStatement ps = connection.prepareStatement(GET_BY_USER_NAME)) {
            ps.setString(1, customer);
            maybeUser = userConverter.convertFromDB(customer, rs = ps.executeQuery());
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return maybeUser;
    }
}

