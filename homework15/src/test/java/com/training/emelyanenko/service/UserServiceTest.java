package com.training.emelyanenko.service;

import com.training.emelyanenko.exception.InvalidArgumentException;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserServiceTest {

	@Test
	public void getExisting() {
		assertEquals( UserService.createOrGet("Stanislav"), UserService.createOrGet("Stanislav"));
	}

	@Test
	public void createAndGet() {
		assertEquals( "Stanislav", UserService.createOrGet("Stanislav").getName());
	}

	@Test(expected = InvalidArgumentException.class)
	public void createOrGetWithException() {
		UserService.createOrGet(null);
	}
}