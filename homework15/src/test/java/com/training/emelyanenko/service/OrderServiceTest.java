package com.training.emelyanenko.service;

import com.training.emelyanenko.domain.Order;
import com.training.emelyanenko.domain.PriceList;
import com.training.emelyanenko.domain.Product;
import com.training.emelyanenko.exception.InvalidArgumentException;
import com.training.emelyanenko.repository.UserRepository;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class OrderServiceTest {



	@Test
	public void createOrder() {
		Order order = OrderService.createOrGetOrder("Stanislav");
		assertEquals(new Order("Stanislav").getCustomer(), order.getCustomer());
	}

	@Test(expected = InvalidArgumentException.class)
	public void createOrderWitExc() {
		Order order = OrderService.createOrGetOrder(null);
	}

	@Test
	public void addProducts() {
		UserService.createOrGet("Stanislav");
		Order expectedOrder = OrderService.createOrGetOrder("Stanislav");
		OrderService.addProducts(UserRepository.getByName("Stanislav").get(), new String[] {"Product1"});
		List<Product> expectedList = new ArrayList<>();
		Product product = new Product();
		product.setName("Product1");
		product.setPrice(PriceList.getPRODUCTS().get("Product1"));
		expectedList.add(product);
		assertEquals(expectedList.get(0).getName(), expectedOrder.getProducts().get(0).getName());
	}

	@Test(expected = InvalidArgumentException.class)
	public void addProductsWithExc() {
		Order expectedOrder = OrderService.createOrGetOrder("Stanislav");
		OrderService.addProducts(null, null);

	}


}