package com.training.emelyanenko.domain;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Class that represents POJO of user.
 */
public class User {

	private static AtomicInteger count = new AtomicInteger(0);
	Order order;
	private int id;
	private String name;

	public User(String name) {
		this.name = name;
		this.id = count.incrementAndGet();
	}

	public int getId() {
		return id;
	}


	public String getName() {
		return name;
	}


	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}
}
