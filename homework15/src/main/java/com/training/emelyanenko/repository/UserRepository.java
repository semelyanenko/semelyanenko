package com.training.emelyanenko.repository;

import com.training.emelyanenko.domain.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public final class UserRepository {

	private static List<User> users = Collections.synchronizedList(new ArrayList<>());

	private UserRepository() {
	}

	/**
	 * Saves user in "data base" (list of orders).
	 *
	 * @param user it's order to save.
	 * @return saved user.
	 */
	public static User save(User user) {
		users.add(user);
		return user;
	}

	/**
	 * Returns user by id if user is exist.
	 *
	 * @param id user id.
	 * @return user by id if user is exist.
	 */
	public static Optional<User> getById(String id) {
		for (User user : users) {
			if (user.getId() == (Integer.parseInt(id))) {
				return Optional.of(user);
			}
		}
		return Optional.empty();
	}

	/**
	 * Returns user by username if user is exist.
	 *
	 * @param customer user name.
	 * @return user by username if user is exist.
	 */
	public static Optional<User> getByName(String customer) {
		for (User user : users) {
			if (user.getName().equals(customer)) {
				return Optional.of(user);
			}
		}
		return Optional.empty();
	}
}
