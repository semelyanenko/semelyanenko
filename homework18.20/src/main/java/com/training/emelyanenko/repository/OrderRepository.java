package com.training.emelyanenko.repository;

import com.training.emelyanenko.converter.OrderConverter;
import com.training.emelyanenko.domain.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Repository class that emulate data base and has some repository methods.
 */
@Repository
public final class OrderRepository {

    private static final String SAVE_QUERY = "INSERT INTO SHOP_ORDER (id, user_id, total_price) VALUES (?,?,?);";
    private static final String UPDATE_TOTAL_PRICE_QUERY = "UPDATE SHOP_ORDER SET total_price = ? WHERE id = ?;";

    private Connection connection;
    private OrderConverter orderConverter;

    @Autowired
    public OrderRepository(Connection connection, OrderConverter orderConverter) {
        this.connection = connection;
        this.orderConverter = orderConverter;
    }

    /**
     * Saves order in database.
     *
     * @param order it's order to save.
     * @return saved order.
     */
    public Order save(Order order) {
        try (PreparedStatement ps = connection.prepareStatement(SAVE_QUERY)) {
            ps.setInt(1, order.getId());
            ps.setInt(2, order.getUserId());
            ps.setDouble(3, order.getTotalPrice());
            ps.execute();
            orderConverter.convert(order);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return order;
    }

    public void changeTotalPrice(Order order) {
        try (PreparedStatement ps = connection.prepareStatement(UPDATE_TOTAL_PRICE_QUERY)) {
            ps.setDouble(1, order.getTotalPrice());
            ps.setInt(2, order.getId());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
