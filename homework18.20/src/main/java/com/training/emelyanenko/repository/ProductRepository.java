package com.training.emelyanenko.repository;

import com.training.emelyanenko.converter.ProductConverter;
import com.training.emelyanenko.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Repository
public final class ProductRepository {

    private static final String SAVE_GOOD_QUERY = "INSERT INTO ORDER_GOOD (ORDER_ID, GOOD_ID) VALUES (?,?)";
    private static final String GET_ALL_GOODS_QUERY = "SELECT * FROM GOOD";
    private static final String GET_GOOD_ID_QUERY = "SELECT * FROM GOOD WHERE TITLE =?";
    private Connection connection;
    private ProductConverter productConverter;

    @Autowired
    public ProductRepository(Connection connection, ProductConverter productConverter) {
        this.connection = connection;
        this.productConverter = productConverter;
    }

    public void saveOrderGood(Product product, int orderId) {
        try (PreparedStatement ps = connection.prepareStatement(SAVE_GOOD_QUERY)) {
            ps.setInt(1, orderId);
            ps.setInt(2, product.getId());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Map<String, Double> getAll() {
        Map<String, Double> products = new HashMap<>();
        try (PreparedStatement ps = connection.prepareStatement(GET_ALL_GOODS_QUERY); ResultSet rs = ps.executeQuery()) {
            products = productConverter.convertAll(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return products;

    }

    public int getIdByName(String productName) {
        int productId = -1;
        try (PreparedStatement ps = connection.prepareStatement(GET_GOOD_ID_QUERY)) {
            ps.setString(1, productName);
            ResultSet rs = ps.executeQuery();
            productId = productConverter.convert(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productId;
    }
}

