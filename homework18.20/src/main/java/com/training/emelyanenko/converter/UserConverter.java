package com.training.emelyanenko.converter;

import com.training.emelyanenko.domain.Order;
import com.training.emelyanenko.domain.Product;
import com.training.emelyanenko.domain.User;
import com.training.emelyanenko.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class UserConverter {

    private OrderRepository orderRepository;

    @Autowired
    public UserConverter(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public Optional<User> convertFromDB(String customer, ResultSet rs) {
        Optional<User> maybeUser = Optional.empty();
        try {
            List<Product> products = new ArrayList<>();
            if (!rs.next()) {
                return Optional.empty();
            }
            User user = new User(customer);
            user.setId(rs.getInt("id"));
            user.setPassword(rs.getString("password"));
            Product product;
            Order order = new Order(user.getName());
            order.setUserId((rs.getInt("user_id")));
            order.setCustomer(user.getName());
            order.setId(rs.getInt("order_id"));
            order.setTotalPrice(rs.getDouble("total_price"));
            do {
                if (rs.getString("title") == null) {
                    continue;
                }
                product = new Product();
                product.setName(rs.getString("title"));
                product.setPrice(rs.getDouble("price"));
                product.setId(rs.getInt("good_id"));
                products.add(product);
            } while (rs.next());
            order.setProducts(products);
            user.setOrder(order);
            maybeUser = Optional.of(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return maybeUser;
    }

    public void convertToDB(User user) {
        user.getOrder().setUserId(user.getId());
        orderRepository.save(user.getOrder());
    }
}
