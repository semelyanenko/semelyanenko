package com.training.emelyanenko.controller;

import com.training.emelyanenko.service.OrderService;
import com.training.emelyanenko.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
public final class LoginController {

    private OrderService orderService;
    private ProductService productService;

    @Autowired
    public LoginController(OrderService orderService, ProductService productService) {
        this.orderService = orderService;
        this.productService = productService;
    }

    /**
     * Аnalogue of welcome page in servlet app.
     *
     * @return login view.
     */
    @GetMapping("/")
    public String goToLoginPage() {
        return "login";
    }

    @RequestMapping("/login")
    public String login(Principal principal, ModelMap model) {

        if (principal != null) {
            model.addAttribute("products", productService.getPriceList());
            model.addAttribute("order", orderService.getOrder(principal.getName()));
            return "product";
        } else {
            return "login";
        }
    }

    @PostMapping("/logout")
    public String logout() {
        return "redirect:/login?logout";
    }
}
