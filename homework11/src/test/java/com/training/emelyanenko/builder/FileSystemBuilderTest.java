package com.training.emelyanenko.builder;

import com.training.emelyanenko.exception.IncorrectRootException;
import com.training.emelyanenko.system.FileSystem;
import org.junit.Before;
import org.junit.Test;

public class FileSystemBuilderTest {

    FileSystem fileSystem = new FileSystem();
    FileSystemBuilder systemBuilder = new FileSystemBuilder(fileSystem);

    @Before
    public void setUp() {
        systemBuilder.build("root/folder1");
    }

    @Test
    public void build() {
        systemBuilder.build("root/folder2");
    }

    @Test(expected = IncorrectRootException.class)
    public void buil_withIncorrectRootException() {
        systemBuilder.build("root1/folder");
    }
}