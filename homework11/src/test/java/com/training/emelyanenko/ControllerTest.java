package com.training.emelyanenko;

import org.junit.Test;

import static org.junit.Assert.*;

public class ControllerTest {

    @Test(expected = RuntimeException.class)
    public void testControl_withRuntimeException() {
        Controller controller = new Controller();
        controller.control("root\\folder");
    }

    @Test
    public void control() {
        Controller controller = new Controller();
        controller.control("root/folder");
    }
}