package com.training.emelyanenko.system;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Class which represents a file system.
 */
public class FileSystem implements Serializable {
    private static final long serialVersionUID = 1L;

    private String rootFolder;
    private List<Folder> folders = new ArrayList<>();

    public FileSystem() {
    }

    public String getRootFolder() {
        return rootFolder;
    }

    public void setRootFolder(String rootFolder) {
        this.rootFolder = rootFolder;
    }

    /**
     * @return ArrayList of folders.
     */
    public List<Folder> getFolders() {
        return folders;
    }

    /**
     * Adds folder in file system root folder.
     *
     * @param folder folder to add.
     */
    public void addFolder(Folder folder) {
        if (folder == null) {
            folder = new Folder("unnamed");
        }
        folders.add(folder);
    }

}
