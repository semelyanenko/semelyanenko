package com.training.emelyanenko.exception;

public class IncorrectRootException extends RuntimeException {

    public IncorrectRootException(String message) {
        super(message);
    }
}
