package com.training.emelyanenko.util;


import com.training.emelyanenko.system.FileSystem;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Util class for file system serialization and deserialization.
 */
public class Serializer {

    private Serializer() {
    }


    /**
     * @param fs file system.
     */
    public static void serialize(FileSystem fs) {
        if (fs == null) {
            throw new RuntimeException("File system can't be null");
        }
        try (FileOutputStream fos = new FileOutputStream("temp.out");
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(fs);
            oos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Deserializes file system form file.
     * Returns empty file system if if deserialization error occurred.
     *
     * @return file system.
     */
    public static FileSystem deSerialize() {
        FileSystem fs = new FileSystem();
        try (FileInputStream fis = new FileInputStream("temp.out");
             ObjectInputStream oin = new ObjectInputStream(fis)) {
            fs = (FileSystem) oin.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return fs;
    }
}