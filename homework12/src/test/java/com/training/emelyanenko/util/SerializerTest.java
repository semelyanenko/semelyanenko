package com.training.emelyanenko.util;

import com.training.emelyanenko.system.FileSystem;
import com.training.emelyanenko.system.Folder;
import org.junit.Test;

public class SerializerTest {

    FileSystem fileSystem;

    @Test
    public void serialize() {
        fileSystem = new FileSystem();
        fileSystem.setRootFolder("root");
        fileSystem.addFolder(new Folder("folder"));
        Serializer.serialize(fileSystem);
    }

    @Test(expected = RuntimeException.class)
    public void serialize_withRuntimeException() {
        Serializer.serialize(null);
    }

    @Test
    public void deSerialize() {
        FileSystem savedSystem = Serializer.deSerialize();
    }
}