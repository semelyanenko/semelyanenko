package com.training.emelyanenko;

import com.training.emelyanenko.exception.IncorrectRootException;
import org.junit.Test;

import static org.junit.Assert.*;

public class ControllerTest {

    @Test
    public void control() {
        Controller controller = new Controller();
        controller.control("root/folder");
    }

    @Test(expected = IncorrectRootException.class)
    public void controlWithExaption() {
        Controller controller = new Controller();
        controller.control("//");
    }

}