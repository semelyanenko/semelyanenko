package com.training.emelyanenko.util;

import com.training.emelyanenko.system.File;
import com.training.emelyanenko.system.FileSystem;
import com.training.emelyanenko.system.Folder;

import java.util.List;

/**
 * Util class for printing file system in console.
 */
public class Printer {

    private static final String FOLDER_TAB = "      ";
    private static final String FILE_TAB = "   ";

    private Printer() {
    }


    /**
     * General method for printing.
     *
     * @param fileSystem it's file system to print.
     */
    public static void print(FileSystem fileSystem) {
        System.out.println(fileSystem.getRootFolder());
        List<Folder> folders = fileSystem.getFolders();
        for (Folder f : folders) {
            printFolders(f, FOLDER_TAB);
        }
    }

    /**
     * @param outFolder it' outer folder.
     * @param tab       it's tabulation.
     */
    private static void printFolders(Folder outFolder, String tab) {
        System.out.println(tab + outFolder.getName() + "/");
        printFiles(outFolder, tab + FILE_TAB);
        List<Folder> folders = outFolder.getFolders();
        for (Folder folder : folders) {
            printFolders(folder, tab + FOLDER_TAB);
        }

    }

    /**
     * @param folder current folder.
     * @param tab    it's tabulation.
     */
    private static void printFiles(Folder folder, String tab) {
        List<File> files = folder.getFiles();
        for (File file : files) {
            System.out.println(FILE_TAB + tab + file.getFullName());
        }
    }
}
