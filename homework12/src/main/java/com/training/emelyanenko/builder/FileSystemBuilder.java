package com.training.emelyanenko.builder;

import com.training.emelyanenko.exception.IncorrectRootException;
import com.training.emelyanenko.system.File;
import com.training.emelyanenko.system.FileSystem;
import com.training.emelyanenko.system.Folder;

import java.util.List;

/**
 * Builder of a file system.
 */
public class FileSystemBuilder {
    private String[] pathArr;
    private FileSystem fs;

    public FileSystemBuilder(FileSystem fs) {
        this.fs = fs;
    }

    public FileSystem getFileSystem() {
        return fs;
    }

    /**
     * @param path just path.
     * @return updated file system.
     */
    public FileSystem build(String path) {
        int index = 1;
        pathArr = path.split("/");
        if (pathArr.length < 2 || (fs.getRootFolder() != null && !fs.getRootFolder().equals(pathArr[0]))) {
            throw new IncorrectRootException("Incorrect root name");
        }
        fs.setRootFolder(pathArr[0]);
        if (fs.getFolders().size() == 0 && pathArr.length >= 1) {
            fs.addFolder(new Folder(pathArr[index]));
            if (index + 1 < pathArr.length) {
                scan(fs.getFolders().get(fs.getFolders().size() - 1), ++index);
            }
        } else {
            List<Folder> fsFolders = fs.getFolders();
            boolean isjumped = false;

            for (Folder folder : fsFolders) {
                if (folder.getName().equals(pathArr[index])) {
                    if (index + 1 < pathArr.length) {
                        isjumped = true;
                        scan(folder, ++index);
                        break;
                    }
                    break;
                }
            }
            if (!isjumped) {
                fs.addFolder(new Folder(pathArr[index]));
            }

        }
        return fs;
    }

    /**
     * Reflection method for scanning and adding folders.
     *
     * @param folder current folder.
     * @param index  it's index of path argument.
     */
    private void scan(Folder folder, int index) {
        for (Folder fold : folder.getFolders()) {
            if (fold.getName().equals(pathArr[index])) {
                if (index + 1 < pathArr.length) {
                    scan(fold, ++index);
                    return;

                }
                return;
            }
        }
        if (index == pathArr.length - 1 && hasFile()) {
            folder.addFile(new File(pathArr[index]));
        } else {
            folder.addFolder(new Folder(pathArr[index]));
        }
        if (index + 1 < pathArr.length) {
            scan(folder.getFolders().get(folder.getFolders().size() - 1), ++index);
        }
        return;
    }

    /**
     * @return true if path has a file.
     */
    private boolean hasFile() {
        return (pathArr[pathArr.length - 1].contains("."));
    }

}
