package com.training.emelyanenko;

import com.training.emelyanenko.builder.FileSystemBuilder;
import com.training.emelyanenko.exception.IncorrectRootException;
import com.training.emelyanenko.system.FileSystem;
import com.training.emelyanenko.util.Printer;
import com.training.emelyanenko.util.Serializer;

/**
 * Class for controlling file system.
 */
public class Controller {

    private String path;
    private FileSystem fs = new FileSystem();
    private FileSystemBuilder fsB = new FileSystemBuilder(fs);
    private String consoleIn;

    public FileSystem getFileSystem() {
        return fs;
    }

    /**
     * @param consoleIn path from console.
     */
    public void control(String consoleIn) {
        this.consoleIn = consoleIn;
        isValid();
        switch (path) {
            case "print":
                Printer.print(fs);
                break;
            case "-fs save":
                Serializer.serialize(fs);
                break;
            case "-fs restore":
                fsB = new FileSystemBuilder(Serializer.deSerialize());
                fs = fsB.getFileSystem();
                System.out.println("File system was restored");
                Printer.print(fs);
                break;
            default:
                this.fs = fsB.build(consoleIn);
                break;
        }
    }

    /**
     * @return true if path is valid.
     */
    private boolean isValid() {
        path = consoleIn;
        if (path == null) {
            throw new RuntimeException("path can't be null");
        }
        String[] fullPath = consoleIn.split("/");
        if (fullPath.length > 1 && fullPath[fullPath.length - 1].contains(".")) {
            path = "";
            for (int i = 0; i < fullPath.length - 1; i++) {
                path = path + fullPath[i] + "/";
            }
        }
        if (!path.matches("(\\/?.+\\/)*(.+?)(?:\\..+)?$")) {
            throw new RuntimeException("Invalid path");
        }
        return true;
    }
}
