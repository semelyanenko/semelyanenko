package com.training.emelyanenko;

import java.util.Scanner;


public class Main {
    /**
     * The program enter.
     *
     * @param args program arguments
     */
    public static void main(String[] args) {
        System.out.println("Insert rootFolderName/folderName");
        String console = "";
        Scanner scanner = new Scanner(System.in);
        console = scanner.nextLine();
        Controller controller = new Controller();
        while (!console.equals("exit")) {
            controller.control(console);
            console = scanner.nextLine();
        }

    }
}

