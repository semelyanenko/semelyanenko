package com.training.emelyanenko.system;

import java.io.Serializable;

/**
 * Class which represents a file.
 */
public class File implements Serializable {
    private static final long serialVersionUID = 1L;

    private String name;
    private String format;

    /**
     * Creates file uses full name as name of file and it format.
     *
     * @param fullName it't full name as "name.format'.
     */
    public File(String fullName) {
        String[] file = fullName.split("[.]");
        this.name = file[0];
        this.format = file[1];
    }

    public String getFullName() {
        return name + "." + format;
    }

}
