package com.training.emelyanenko.system;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Class which represents a folder.
 */
public class Folder implements Serializable {
    private static final long serialVersionUID = 1L;

    private String name;
    private List<File> files = new ArrayList<>();
    private List<Folder> folders = new ArrayList<>();

    public Folder(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    /**
     * @return ArrayList of files.
     */
    public List<File> getFiles() {
        return files;
    }

    /**
     * @return Array list of folders.
     */
    public List<Folder> getFolders() {
        return folders;
    }

    /**
     * @param file file to add.
     */
    public void addFile(File file) {
        if (file == null) {
            file = new File("unknown");
        }
        files.add(file);
    }

    /**
     * @param folder folder to add.
     */
    public void addFolder(Folder folder) {
        if (folder == null) {
            folder = new Folder("unnamed");
        }
        folders.add(folder);
    }

}
