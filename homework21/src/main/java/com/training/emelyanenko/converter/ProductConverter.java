package com.training.emelyanenko.converter;

import com.training.emelyanenko.domain.Product;
import com.training.emelyanenko.dto.ProductDto;
import org.springframework.stereotype.Component;

@Component
public class ProductConverter extends AbstractConverter<Product, ProductDto> {

    @Override
    Class<ProductDto> getDomainClass() {
        return ProductDto.class;
    }

    @Override
    Class<Product> getEntityClass() {
        return Product.class;
    }
}
