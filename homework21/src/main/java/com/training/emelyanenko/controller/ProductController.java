package com.training.emelyanenko.controller;

import com.training.emelyanenko.dto.ProductDto;
import com.training.emelyanenko.service.OrderService;
import com.training.emelyanenko.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@Transactional
@RequestMapping(value = "/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private OrderService orderService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getById(@PathVariable int id) {
        return ResponseEntity.ok(productService.getById(id));
    }

    @GetMapping()
    public List<ProductDto> getAll() {
        return productService.getPriceList();
    }

    @PutMapping("/{productId}")
    public ResponseEntity<?> addToOrder(@PathVariable int productId, Principal principal) {
        orderService.addProductToOrder(principal.getName(), productId);
        return ResponseEntity.ok("You have successfully added product to order");
    }
}
