package com.training.emelyanenko.service;

import com.training.emelyanenko.converter.ProductConverter;
import com.training.emelyanenko.domain.Product;
import com.training.emelyanenko.dto.ProductDto;
import com.training.emelyanenko.exception.ResourceNotFoundException;
import com.training.emelyanenko.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    private ProductRepository productRepository;
    private ProductConverter productConverter;

    @Autowired
    public ProductService(ProductRepository productRepository, ProductConverter productConverter) {
        this.productRepository = productRepository;
        this.productConverter = productConverter;
    }

    public List<ProductDto> getPriceList() {
        List<Product> products = productRepository.getAll();
        List<ProductDto> dtoProducts = new ArrayList<>();
        for (Product product : products) {
            dtoProducts.add(productConverter.toDto(product));
        }
        return dtoProducts;
    }

    public ProductDto getById(int id) {
        Optional<Product> maybeProduct = productRepository.getById(id);
        if (maybeProduct.isPresent()) {
            return productConverter.toDto(maybeProduct.get());
        } else {
            throw new ResourceNotFoundException("Product", "id", id);
        }
    }
}
