<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <style>
        <%@include file="/WEB-INF/css/style.css" %>
    </style>
    <title>Online-shop</title>
</head>
<body>
<form action="logout" method="post">
    <nav>
        <button>logout</button>
    </nav>
</form>
<div class="box">
    <p>Dear ${order.customer}, your order:</p>
    <% int index = 0; %>
    <c:forEach var="pickedProduct" items="${order.products}">
        <p><%=index += 1%>) ${pickedProduct.name} ${pickedProduct.price}$</p>
    </c:forEach>
    <p>Total: ${order.totalPrice}$</p>
</div>
</body>
</html>