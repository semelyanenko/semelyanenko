<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <style>
        <%@include file="/WEB-INF/css/style.css" %>
    </style>
    <title>Online-shop</title>
</head>
<body>
<form action="logout" method="post">
<nav>
    <button>logout</button>
</nav>
</form>
<div class="box">
    <p class="msg">Hello ${order.customer}!</p>
    <p class="msg">Make you order</p>
    <form method="post" action="product">
        <select name="selected">
            <c:forEach var="product" items="${products}">
                <option value="${product.key}">${product.key} (${product.value}$)</option>
            </c:forEach>
        </select>
        <input type="submit" value="Add item"/>
        <input type="hidden" name="id" value="${order.id}"/>
        <input type="hidden" name="customer" value="${order.customer}"/>
    </form>
    <form method="post" action="basket">
        <input type="submit" value="Submit">
        <input type="hidden" name="id" value="${order.id}"/>
        <input type="hidden" name="customer" value="${order.customer}"/>
    </form>
    <p>You have already chosen:</p>
    <c:forEach var="pickedProduct" items="${order.products}">
        <P>${pickedProduct.name} ${pickedProduct.price}</P>
    </c:forEach>
</div>
</body>
</html>

