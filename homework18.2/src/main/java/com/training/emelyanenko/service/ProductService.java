package com.training.emelyanenko.service;

import com.training.emelyanenko.domain.Order;
import com.training.emelyanenko.domain.Product;
import com.training.emelyanenko.exception.InvalidArgumentException;
import com.training.emelyanenko.repository.OrderRepository;
import com.training.emelyanenko.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public final class ProductService {

    private ProductRepository productRepository;
    private OrderService orderService;
    private OrderRepository orderRepository;

    @Autowired
    public ProductService(ProductRepository productRepository, OrderService orderService, OrderRepository orderRepository) {
        this.productRepository = productRepository;
        this.orderService = orderService;
        this.orderRepository = orderRepository;
    }

    /**
     * Adds product to order.
     *
     * @param order            order to add.
     * @param selectedProducts string array of product keys from product map.
     * @return order with saved order.
     */
    public void addProductToOrder(Order order, String[] selectedProducts) {
        if (order == null || selectedProducts == null) {
            throw new InvalidArgumentException("Arguments cant be null");
        }

        List<Product> products = order.getProducts();
        String productName = selectedProducts[0];
        Product product = new Product();
        product.setName(productName);
        product.setPrice(getPriceList().get(productName));
        product.setId(productRepository.getIdByName(productName));
        products.add(product);
        order.setTotalPrice(orderService.calcTotalPrice(order));
        order.setProducts(products);
        orderRepository.changeTotalPrice(order);
        productRepository.saveOrderGood(product, order.getId());
    }

    private Map<String, Double> initMap() {
        return productRepository.getAll();
    }

    public Map<String, Double> getPriceList() {
        return productRepository.getAll();
    }

}
