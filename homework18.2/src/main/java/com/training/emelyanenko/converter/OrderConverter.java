package com.training.emelyanenko.converter;

import com.training.emelyanenko.domain.Order;
import com.training.emelyanenko.domain.Product;
import com.training.emelyanenko.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class OrderConverter {

    private ProductRepository productRepository;

    @Autowired
    public OrderConverter(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public void convert(Order order) {
        for (Product product : order.getProducts()) {
            productRepository.saveOrderGood(product, order.getId());
        }
    }
}
